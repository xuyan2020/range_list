# Directory Structure
- range_list: RangeList script;
- test: unit test folder, written in Rspec.

# Note
1. There are two scripts, one is just implements the given use case, the syntax is relatively simple(range_list_only_test_case.rb). Other one is adds some custom use cases according to the rules found from the preset use cases, which is more comprehensive and has more complex syntax(range_list.rb).
2. There are also two test cases in test folder.
3. Thanks!
