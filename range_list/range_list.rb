# ==========================================
# tips:
# 1. This script adds some custom use cases according to the rules found from the preset use cases, 
#    which is more comprehensive and has more complex syntax.
# 2. In order to reuse code, some methods are implemented recursively.
# ==========================================

class RangeList
  def initialize
    @range_list = []
  end

  # params:
  #   range: new range, for example [1, 2]
  def add(range)
    range.map!{|_r| _r.to_i}.sort!
    _add_range_to_list(range, @range_list)
  end

  # params:
  #   range: new range, for example [1, 2]
  def remove(range)
    range.map!{|_r| _r.to_i}.sort!
    _remove_range_from_list(range)
  end

  def print
    @range_list.map{|_range| "[#{_range.join(', ')})" }.join(" ")
  end

private

  # params:
  #   new_range: new range, for example [1, 2];
  #   tmp_range_list: modify range list
  def _add_range_to_list(new_range, tmp_range_list)
    return unless new_range.uniq.size == new_range.size
    return tmp_range_list << new_range if tmp_range_list.empty?
    _tmp_range_list = Marshal.load(Marshal.dump(tmp_range_list))
    tmp_range_list.clear
    _first_num, _last_num = *new_range
    _tmp_range_list.each do |_range|
      # It is handled according to the position of the two parameters in the original range.
      case [_first_num.between?(_range[0], _range[1]), _last_num.between?(_range[0], _range[1])]
      when [true, true]
        # Both params in current range, delete new_range from tmp_range_list and add current range to tmp_range_list. 
        tmp_range_list.delete(new_range)
        tmp_range_list << _range 
      when [true, false]
        _tmp_rl = [_range[0], _last_num]
        # Need to determine whether the new range and the existing range in the list overlap, same below.
        _add_range_to_list(_tmp_rl, tmp_range_list)
      when [false, true]
        _tmp_rl = [_first_num, _range[1]]
        _add_range_to_list(_tmp_rl, tmp_range_list)
      else
        # If the new range contains the old range, delete the old range.
        if _first_num < _range[0] && _last_num > _range[1]
          tmp_range_list.delete(_range)
          _add_range_to_list(new_range, tmp_range_list)
          next
        end
        # Otherwise, the new range does not include the old range, and both the new range and the old range must be added to the list.
        _add_range_to_list(new_range, tmp_range_list)
        tmp_range_list << _range
      end
    end
    tmp_range_list.uniq!
    tmp_range_list.sort_by!{|_range| _range[0]}
  end

  # params:
  #   rm_range: remove range, for example [1, 2]
  def _remove_range_from_list(rm_range)
    return if @range_list.empty? || rm_range.uniq.size != rm_range.size
    _first_num, _last_num = *rm_range
    new_range_list = []
    @range_list.each do |_range|
      # If both the new and old range not repeat, don't remove anything.
      if _first_num > _range[1] || _last_num < _range[0]
        new_range_list << _range
        next
      end
      case [_first_num < _range[0], _last_num > _range[1]]
      when [true, false]
        _tmp_range = _last_num <= _range[0] ? _range : [_last_num, _range[1]]
        new_range_list << _tmp_range
      when [false, true]
        next if _first_num == _range[0]
        new_range_list << [_range[0], _first_num]
      when [false, false]
        # The following three cases are dealt with separately.
        next if _first_num == _range[0] && _last_num == _range[1]
        new_range_list << [_range[0], _first_num] unless _range[0] == _first_num
        new_range_list << [_last_num, _range[1]] unless _last_num == _range[1]
      end
    end
    @range_list = new_range_list.uniq.sort_by{|_range| _range[0]}
  end
end

