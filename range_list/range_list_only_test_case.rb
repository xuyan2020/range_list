# ==========================================
# tips:
# This script just implements the given use case, the syntax is relatively simple.
# ==========================================

class RangeList
  def initialize
    @range_list = []
  end

  # params:
  #   range: new range, for example [1, 2]
  def add(range)
    range.map!{|_r| _r.to_i}.sort!
    return @range_list << range if @range_list.empty?
    return if range.uniq.size != range.size
    # Define a repeat flag:
    #   true: new range and other ranges have intersection points;
    #   false: new range and other ranges not have intersection points.
    repeat_flag = false
    _new_range_list = []
    _first_num, _last_num = *range
    @range_list.map! do |_range|
      # It is handled according to the position of the two parameters in the original range.
      case [_first_num.between?(_range[0], _range[1]), _last_num.between?(_range[0], _range[1])]
      when [true, false]
        repeat_flag = true
        _new_range_list.delete(range)
        [_range[0], _last_num]
      when [false, true]
        repeat_flag = true
        _new_range_list.delete(range)
        [_first_num, _range[1]]
      when [false, false]
        # The new range is added when it does not intersect any range that has already been verified.
        _new_range_list << range if !repeat_flag && (_first_num > _range[1] || _last_num < _range[0])
        _range
      else
        repeat_flag = true
        _new_range_list.delete(_range)
        _range
      end
    end
    @range_list += _new_range_list
    @range_list.sort_by!{|_range| _range[0]}.uniq!
  end

  # This method is same with other script.
  # params:
  #   range: remove range, for example [1, 2]
  def remove(range)
    range.map!{|_r| _r.to_i}.sort!
    return if @range_list.empty? || range.uniq.size != range.size
    _first_num, _last_num = *range
    new_range_list = []
    @range_list.each do |_range|
      if _first_num > _range[1] || _last_num < _range[0]
        new_range_list << _range
        next
      end
      case [_first_num < _range[0], _last_num > _range[1]]
      when [true, false]
        _tmp_range = _last_num <= _range[0] ? _range : [_last_num, _range[1]]
        new_range_list << _tmp_range
      when [false, true]
        next if _first_num == _range[0]
        new_range_list << [_range[0], _first_num]
      when [false, false]
        next if _first_num == _range[0] && _last_num == _range[1]
        new_range_list << [_range[0], _first_num] unless _range[0] == _first_num
        new_range_list << [_last_num, _range[1]] unless _last_num == _range[1]
      end
    end
    @range_list = new_range_list.uniq.sort_by{|_range| _range[0]}
  end

  def print
    @range_list.map{|_range| "[#{_range.join(', ')})" }.join(" ")
  end
end
