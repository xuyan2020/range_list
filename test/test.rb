require File.expand_path('../../range_list/range_list', __FILE__)

describe RangeList do
  context "test RangeList class methods" do
    before(:all) do 
      @rl = RangeList.new
    end

    # add test cases
    it "test add [1, 5]" do
      @rl.add([1, 5])
      ret = @rl.print
      expect(ret).to eql("[1, 5)")
    end

    it "test add [10, 20]" do
      @rl.add([10, 20])
      ret = @rl.print
      expect(ret).to eql("[1, 5) [10, 20)")
    end

    it "test add [20, 20]" do
      @rl.add([20, 20])
      ret = @rl.print
      expect(ret).to eql("[1, 5) [10, 20)")
    end

    it "test add [20, 21]" do
      @rl.add([20, 21])
      ret = @rl.print
      expect(ret).to eql("[1, 5) [10, 21)")
    end

    it "test add [2, 4]" do
      @rl.add([2, 4])
      ret = @rl.print
      expect(ret).to eql("[1, 5) [10, 21)")
    end

    it "test add [3, 8]" do
      @rl.add([3, 8])
      ret = @rl.print
      expect(ret).to eql("[1, 8) [10, 21)")
    end

    # remove test cases
    it "test remove [10, 10]" do
      @rl.remove([10, 10])
      ret = @rl.print
      expect(ret).to eql("[1, 8) [10, 21)")
    end

    it "test remove [10, 11]" do
      @rl.remove([10, 11])
      ret = @rl.print
      expect(ret).to eql("[1, 8) [11, 21)")
    end

    it "test remove [15, 17]" do
      @rl.remove([15, 17])
      ret = @rl.print
      expect(ret).to eql("[1, 8) [11, 15) [17, 21)")
    end

    it "test remove [3, 19]" do
      @rl.remove([3, 19])
      ret = @rl.print
      expect(ret).to eql("[1, 3) [19, 21)")
    end

    # Custom test cases
    it "test add [3, 19]" do
      @rl.add([3, 19])
      ret = @rl.print
      expect(ret).to eql("[1, 21)")
    end

    it "test remove [1, 2]" do
      @rl.remove([1, 2])
      ret = @rl.print
      expect(ret).to eql("[2, 21)")
    end

    it "test add [22, 23]" do
      @rl.add([22, 23])
      ret = @rl.print
      expect(ret).to eql("[2, 21) [22, 23)")
    end

    it "test add [100, 199]" do
      @rl.add([100, 199])
      ret = @rl.print
      expect(ret).to eql("[2, 21) [22, 23) [100, 199)")
    end

    it "test add [1, 201]" do
      @rl.add([1, 201])
      ret = @rl.print
      expect(ret).to eql("[1, 201)")
    end
  end
end
